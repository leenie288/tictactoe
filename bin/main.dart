import 'dart:io';

void main() {
  String objectGame;
  String line = '';
  int breakIf = 0;
  int firstPlayerWin = 0;
  int twoPlayerWin = 0;
  print('Hello');
  print('cell number where we write');
  do {
    final List<String> playingField = List.generate(9, (int index) => '_');
    do {
      breakIf = 0;
      print('move x');
      objectGame = 'x';
      printPlayingField(playingField);
      playerTurn(playingField, objectGame);
      printPlayingField(playingField);
      breakIf = combinations(playingField, objectGame, breakIf);
      if (breakIf == 1) {
        firstPlayerWin++;
        break;
      }
      breakIf = draw(playingField, breakIf);
      if (breakIf == 1) {
        break;
      }
      breakIf = 0;
      print('move zero');
      objectGame = 'o';
      printPlayingField(playingField);
      playerTurn(playingField, objectGame);
      printPlayingField(playingField);
      breakIf = combinations(playingField, objectGame, breakIf);
      breakIf = draw(playingField, breakIf);
      if(breakIf == 1){
        twoPlayerWin++;
      }
    } while (combinations(playingField, objectGame, breakIf) == 0 || draw(playingField, breakIf) == 0);
    print('Do you want to play again? 1 - no, - 0 - yes');
    print('Player x: ${firstPlayerWin}, Player o: ${twoPlayerWin} ');
    line = stdin.readLineSync()!;
  } while (line == '0');
  print('Game off, bye');
}

void printPlayingField(List<String> playingField) {
  int index = 0;
  for (int j = 0; j < 3; j++) {
    print('${playingField[index]}|${playingField[index + 1]}|${playingField[index + 2]}');
    index = index + 3;
  }
}

void playerTurn(List<String> playingFields, String objectGame) {
  int loopExit = 0;
  String inputLine = '';
  int cellSelection = 0;
  do {
    inputLine = stdin.readLineSync()!;
    cellSelection = int.parse(inputLine);
    if ((playingFields[cellSelection - 1] != 'x' && playingFields[cellSelection - 1] != 'o')) {
      playingFields[cellSelection - 1] = objectGame;
      loopExit = 0;
    } else if (playingFields[cellSelection - 1] == 'x' || playingFields[cellSelection - 1] == 'o') {
      print('Cell is busy, try again');
      loopExit = 1;
    }
  } while (loopExit == 1);
}

int combinations(List<String> playingField, String objectGame, int winIf) {
  int index = 0;
  winIf = 0;
  for (int i = 0; i < 3; i++) {
    if (playingField[index] == objectGame && playingField[index] == playingField[index + 1] && playingField[index + 2] == playingField[index]) {
      winIf = 1;
      print('win ${objectGame}');
      break;
    }
    index = index + 3;
  }
  index = 0;
  for (int i = 0; i < 2; i++) {
    if (playingField[index] == objectGame && playingField[index] == playingField[index + 3] && playingField[index + 6] == playingField[index]) {
      winIf = 1;
      print('win ${objectGame}');
      break;
    }

    index++;
  }
  index = 0;

  if (playingField[0] == objectGame && playingField[4] == playingField[0] && playingField[8] == playingField[0]) {
    winIf = 1;
    print('win ${objectGame}');
  }
  if (playingField[2] == objectGame && playingField[4] == playingField[2] && playingField[6] == playingField[2]) {
    winIf = 1;
    print('win ${objectGame}');
  }
  return winIf;
}

int draw(List<String> playingField, int breakIf) {
  breakIf = 1;
  for (int i = 0; i < playingField.length; i++) {
    if (playingField[i] == '_') {
      breakIf = 0;
      break;
    }
  }
  if (breakIf == 1) {
    print('the whole field is filled! Draw');
    print('! Draw');
  }

  return breakIf;
}
